package pl.klkl.smx.examples.beans;

import org.apache.camel.language.XPath;

/**
 * User: Michał Lewandowski
 * Date: 22.07.2013
 * Time: 21:13
 */
public class ValidateTouK {
    public boolean validateTouk(@XPath("//Company") String company){
        return "TouK".equals(company);
    }
}
