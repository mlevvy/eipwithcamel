package pl.klkl.smx.examples.beans;

import org.apache.camel.RoutingSlip;
import org.apache.camel.language.XPath;


public class RoutingSlipListBean {
    @RoutingSlip
    public String calculateRoutingSlip(@XPath("//Company")String customer){
        String answer = "log:a";
        if ("TouK".equals(customer)) {
            answer += ",log:b";
        }
        answer += ",log:c";
        return answer;


    }

}