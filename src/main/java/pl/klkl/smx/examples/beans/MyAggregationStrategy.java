package pl.klkl.smx.examples.beans;

import org.apache.camel.Exchange;
import org.apache.camel.builder.xml.XPathBuilder;
import org.apache.camel.processor.aggregate.AggregationStrategy;

/**
 * User: Michał Lewandowski
 * Date: 20.07.2013
 * Time: 19:06
 */
public class MyAggregationStrategy implements AggregationStrategy {
    XPathBuilder xPathBuilder = XPathBuilder.xpath("//Name",String.class);

    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        newExchange.getIn().setBody(getOldBody(oldExchange) + getNewBody(newExchange));
        return newExchange;
    }

    private String getNewBody(Exchange newExchange) {
        return xPathBuilder.evaluate(newExchange,String.class) + "; ";
    }

    private String getOldBody(Exchange oldExchange) {
        if(oldExchange == null){
            return "";
        }else{
            return oldExchange.getIn().getBody(String.class);
        }
    }
}
