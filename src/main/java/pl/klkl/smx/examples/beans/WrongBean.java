package pl.klkl.smx.examples.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Michał Lewandowski
 * Date: 21.07.2013
 * Time: 10:42
 */
public class WrongBean {
    Logger logger = LoggerFactory.getLogger(WrongBean.class);

    public void process(){
        logger.info("Trying very hard to complete process");
        throw new NullPointerException("But, nullpointers, nullpointers everywhere !");
    }

}
