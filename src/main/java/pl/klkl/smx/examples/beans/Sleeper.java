package pl.klkl.smx.examples.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Michał Lewandowski
 * Date: 20.07.2013
 * Time: 19:06
 */
public class Sleeper {
    Logger log = LoggerFactory.getLogger(Sleeper.class);

    public void sleep() throws InterruptedException {
        Thread.sleep(1500);
        log.info("OOoo sie wyspalem");
    }

}
