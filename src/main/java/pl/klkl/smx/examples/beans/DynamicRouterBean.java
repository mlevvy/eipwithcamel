package pl.klkl.smx.examples.beans;

import org.apache.camel.DynamicRouter;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.language.XPath;

/**
 * User: Michał Lewandowski
 */
public class DynamicRouterBean {

    @DynamicRouter
    public String calculateDynamicRouter(@XPath("//Company")String customer,  @Header(Exchange.SLIP_ENDPOINT) String previous){
        if (previous == null) {
            return "log://A";
        } else if ("log://A".equals(previous) && "TouK".equals(customer)) {
            return "log://B";
        } else {
            return null;
        }


    }

}
