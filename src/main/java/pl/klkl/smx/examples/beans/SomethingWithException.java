package pl.klkl.smx.examples.beans;

/**
 * User: Michał Lewandowski
 * Date: 20.07.2013
 * Time: 15:28
 */
public class SomethingWithException {
    public void doSomething() throws InterruptedException {
        Thread.sleep(100);
        throw new RuntimeException("Uppps. Exchecption happens...");
    }
}
