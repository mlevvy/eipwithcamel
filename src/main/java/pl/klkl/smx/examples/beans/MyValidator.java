package pl.klkl.smx.examples.beans;

import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.Property;
import org.apache.camel.language.XPath;

/**
 * User: Michał Lewandowski
 */
public class MyValidator {

    public boolean isValidByString(String element, Exchange exchange, @Header("CamelFileName") String fileName, @Property("CamelBatchComplete") String complete){
        return element.contains("TouK");
    }

    public boolean isValidByXpath(@XPath("//Company") String element){
        return "TouK".equals(element);
    }
}
