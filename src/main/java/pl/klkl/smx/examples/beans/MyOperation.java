package pl.klkl.smx.examples.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Michał Lewandowski
 * Date: 20.07.2013
 * Time: 14:17
 */
public class MyOperation {
    Logger log = LoggerFactory.getLogger(MyOperation.class);

    public void looooongProcesing(String element) throws InterruptedException {
        Thread.sleep(10000);
        log.info("Uff long operation completed...");
    }
}
