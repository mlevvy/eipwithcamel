package pl.klkl.smx.examples.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;
import pl.klkl.smx.examples.beans.MyBean;

/**
 * User: Michał Lewandowski
 * Date: 17.07.2013
 * Time: 21:26
 */
public class MyRouteBuilder extends RouteBuilder {

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    public void configure() {
        //from("timer://myTimer?fixedRate=true&period=5000").bean(MyBean.class).to("log:MyLog");
        from("file://data2?noop=true").to("log:MyLog");

    }

}
