<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes" />

    <xsl:template match="/">
        <nowyXML>
            <zXMLa><xsl:value-of select="//Company"/></zXMLa>
        </nowyXML>
    </xsl:template>
</xsl:stylesheet>